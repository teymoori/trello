package yaico.android.trello.timeDetail;

import java.util.List;

import yaico.android.trello.utils.webmodels.BoardsModel;
import yaico.android.trello.utils.webmodels.MemberModel;

/**
 * Created by amirhossein on 12/18/17.
 */

public interface TimeDetailContract {
    //responses from presenter
    interface View {
        void onPageInit(List<MemberModel> users);

        void onDataLoadError(String msg);

        void onProgress(Boolean show);
        void onFinish() ;

    }

    //requests from activity
    interface Presenter {
        void attachView(TimeDetailContract.View view);

        void onScreenShow();
        void onUpdateClicked();

        void loadData(List<BoardsModel> board);

        void onBoardSelect(BoardsModel board);

        void showError(String msg);

        void onBackClicked() ;
    }

}
