package yaico.android.trello.timeDetail;

import android.util.ArrayMap;
import android.util.Log;

import com.orhanobut.hawk.Hawk;

import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import yaico.android.trello.utils.BaseClass;
import yaico.android.trello.utils.webmodels.MemberModel;

/**
 * Created by amirhossein on 12/18/17.
 */

public class TimeDetailModel extends BaseClass {

    TimeDetailContract.Presenter presenter;

    public TimeDetailModel(TimeDetailContract.Presenter presenter) {
        this.presenter = presenter;
    }


    public List<MemberModel> getUsers() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<MemberModel> memberRealm = realm.where(MemberModel.class).findAll();
//        for (MemberModel memberModel : memberRealm) {
//        }
        return memberRealm;
    }


    public void getData() {

    }


}
