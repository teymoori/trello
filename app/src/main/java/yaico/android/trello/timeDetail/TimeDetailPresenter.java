package yaico.android.trello.timeDetail;

import java.util.List;

import yaico.android.trello.dashboard.DashboardContract;
import yaico.android.trello.utils.webmodels.BoardsModel;

/**
 * Created by amirhossein on 12/18/17.
 */

public class TimeDetailPresenter implements TimeDetailContract.Presenter {

    private TimeDetailContract.View view;
    private TimeDetailModel model = new TimeDetailModel(this);

    @Override
    public void attachView(TimeDetailContract.View view) {
        this.view = view;
    }

    @Override
    public void onScreenShow() {
        view.onPageInit(model.getUsers());
    }

    @Override
    public void onBackClicked() {
        view.onFinish();
    }

    @Override
    public void onUpdateClicked() {

    }

    @Override
    public void loadData(List<BoardsModel> board) {

    }

    @Override
    public void onBoardSelect(BoardsModel board) {

    }

    @Override
    public void showError(String msg) {

    }
}
