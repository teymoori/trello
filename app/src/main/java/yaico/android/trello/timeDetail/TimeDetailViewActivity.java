package yaico.android.trello.timeDetail;

import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.yarolegovich.discretescrollview.DiscreteScrollView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import yaico.android.trello.R;
import yaico.android.trello.utils.BaseActivity;
import yaico.android.trello.utils.Gen;
import yaico.android.trello.utils.webmodels.MemberModel;

@EActivity(R.layout.activity_time_detail_view)
public class TimeDetailViewActivity extends BaseActivity implements TimeDetailContract.View {
    TimeDetailPresenter presenter;
    @ViewById
    DiscreteScrollView usersList;

    @AfterViews
    void afterViews() {
        presenter = new TimeDetailPresenter();
        presenter.attachView(this);
        presenter.onScreenShow();
    }

    @Click
    void back() {
        presenter.onBackClicked();
    }

    @Override
    public void onPageInit(List<MemberModel> members) {

        for (MemberModel memberModel : members)
            Log.d(TAG, "onPageInit_: " + memberModel.toString());

        TimeDetailMembersAdapter adapter = new TimeDetailMembersAdapter(members);
        usersList.setAdapter(adapter);
        Gen.initVDiscreteScrollView(usersList);

    }

    @Override
    public void onDataLoadError(String msg) {

    }

    @Override
    public void onProgress(Boolean show) {

    }

    @Override
    public void onFinish() {
        finish();
    }
}
