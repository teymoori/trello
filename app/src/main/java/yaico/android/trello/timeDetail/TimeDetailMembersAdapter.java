package yaico.android.trello.timeDetail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;
import com.orhanobut.hawk.Hawk;

import java.util.HashMap;
import java.util.List;

import yaico.android.trello.R;
import yaico.android.trello.utils.Gen;
import yaico.android.trello.utils.views.MyTextView;
import yaico.android.trello.utils.webmodels.MemberModel;

/**
 * Created by amirhossein on 12/16/17.
 */

public class TimeDetailMembersAdapter extends RecyclerView.Adapter<TimeDetailMembersAdapter.ViewHolder> {

    private List<MemberModel> members;
    Context mContext;

    public TimeDetailMembersAdapter(List<MemberModel> members) {
        this.members = members;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = inflater.inflate(R.layout.time_users_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.username.setText(  members.get(position).getUserName());

        String imgURL = Gen.getGravatarAddress(members.get(position).getHashAvatar());

        Glide.with(mContext).load(imgURL)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.avatr);

    }

    @Override
    public int getItemCount() {
        return members.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        protected RoundedImageView avatr;
        protected MyTextView username;

        public ViewHolder(View itemView) {
            super(itemView);
            avatr = itemView.findViewById(R.id.avatr);
            username = itemView.findViewById(R.id.username);
        }
    }
}