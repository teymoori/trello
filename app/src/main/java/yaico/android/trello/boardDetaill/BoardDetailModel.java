package yaico.android.trello.boardDetaill;

import android.util.Log;

import java.util.ArrayList;

import io.reactivex.schedulers.Schedulers;
import yaico.android.trello.utils.BaseClass;
import yaico.android.trello.utils.Gen;
import yaico.android.trello.utils.MyApplication;
import yaico.android.trello.utils.webmodels.CardModel;
import yaico.android.trello.utils.webmodels.PList;
import yaico.android.trello.utils.webmodels.ProjectListModel;

/**
 * Created by amirhossein on 12/16/17.
 */

public class BoardDetailModel extends BaseClass {
    BoardDetailPresenter presenter;
    private ProjectListModel projectListModel;

    BoardDetailModel(BoardDetailPresenter presenter) {
        this.presenter = presenter;
    }

    void getDetail(String shortLink) {

        MyApplication.trello.getListsOfProject(shortLink, Gen.getKey(), Gen.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(this::onDataReceived, this::onError, this::onComplete);

    }


    private void getCardsOfList(PList list) {
        try {
            ArrayList<CardModel> card = MyApplication.trello.getCardsOfListID(list.getId(), Gen.getKey(), Gen.getToken()).execute().body();
            list.setCards(card);
        } catch (Exception e) {
            Log.d(TAG, "getCardsOfList: " + e);
        }

    }


    private void onError(Throwable throwable) {
        presenter.showError(throwable.toString());
    }

    private void onDataReceived(ProjectListModel projectListModel) {
        this.projectListModel = projectListModel;
        for (PList list : projectListModel.getLists()) {
            getCardsOfList(list);
        }


        presenter.loadData(projectListModel);
    }

    private void onComplete() {
        Log.d(TAG, "onComplete");
    }
}
