package yaico.android.trello.boardDetaill;

import yaico.android.trello.utils.webmodels.ProjectListModel;

/**
 * Created by amirhossein on 12/16/17.
 */

public class BoardDetailPresenter implements BoardDetailContract.Presenter {
    BoardDetailModel model;
    private BoardDetailContract.View view;

    @Override
    public void attachView(BoardDetailContract.View view) {
        model = new BoardDetailModel(this);
        this.view = view;
    }

    @Override
    public void onScreenShow(String shortLink) {
        view.onProgress(true);
        model.getDetail(shortLink);
    }
    @Override
    public void loadData(ProjectListModel projectListModel) {
        view.onDataLoaded(projectListModel);
        view.onProgress(false);
    }

    @Override
    public void onBackClicked() {
        view.onFinish();
    }

    @Override
    public void showError(String msg) {
        view.onDataLoadError(msg);
        view.onProgress(false);
    }
}
