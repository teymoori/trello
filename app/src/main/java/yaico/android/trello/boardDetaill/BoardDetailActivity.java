package yaico.android.trello.boardDetaill;

import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import yaico.android.trello.R;
import yaico.android.trello.utils.BaseActivity;
import yaico.android.trello.utils.Gen;
import yaico.android.trello.utils.views.MyTextView;
import yaico.android.trello.utils.webmodels.ProjectListModel;

@EActivity(R.layout.activity_board_detail)
public class BoardDetailActivity extends BaseActivity implements BoardDetailContract.View {
    @ViewById
    MyTextView title;
    @ViewById
    DiscreteScrollView list;
    BoardDetailContract.Presenter presenter;
    private DetailAdapter adapter;
    private String shortLink;

    @Extra("shortLink")
    public void setShortLink(String shortLink) {
        this.shortLink = shortLink;
    }

    @AfterViews
    void afterView() {
        presenter = new BoardDetailPresenter();
        presenter.attachView(this);
        presenter.onScreenShow(shortLink);
    }

    @Override
    @UiThread
    public void onDataLoaded(ProjectListModel projectListModel) {
        title.setText(projectListModel.getName());
        adapter = new DetailAdapter(projectListModel.getLists());
        list.setAdapter(adapter);
        Gen.initVDiscreteScrollView(list);
    }

    @Override
    @UiThread
    public void onDataLoadError(String msg) {
        Gen.showToast(mContext, msg);
    }

    @Override
    public void onProgress(Boolean show) {
    }

    @Override
    public void onFinish() {
        finish();
    }

    @Click
    void back() {
        presenter.onBackClicked();
    }
}
