package yaico.android.trello.boardDetaill;

import yaico.android.trello.utils.webmodels.ProjectListModel;

/**
 * Created by amirhossein on 12/12/17.
 */

public interface BoardDetailContract {

    //responses from presenter
    interface View {

        void onDataLoaded(ProjectListModel projectListModel);
        void onDataLoadError(String msg);

        void onProgress(Boolean show);
        void onFinish() ;
    }


    //requests from activity
    interface Presenter {
        void attachView(View view);

        void onScreenShow(String shortLink);

        void showError(String msg);
        void loadData(ProjectListModel projectListModel);
        void onBackClicked() ;
    }
}
