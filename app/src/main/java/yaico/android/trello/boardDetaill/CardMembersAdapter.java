package yaico.android.trello.boardDetaill;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;
import com.orhanobut.hawk.Hawk;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import yaico.android.trello.R;
import yaico.android.trello.utils.Gen;
import yaico.android.trello.utils.webmodels.MemberModel;

/**
 * Created by amirhossein on 12/16/17.
 */

public class CardMembersAdapter extends RecyclerView.Adapter<CardMembersAdapter.ViewHolder> {

    private List<String> card;
    private Context mContext;
    private Realm realm = Realm.getDefaultInstance();

    public CardMembersAdapter(List<String> card) {
        this.card = card;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = inflater.inflate(R.layout.card_members_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String imgURL = Gen.getGravatarAddress(getImageHash(card.get(position)));
        Glide.with(mContext).load(imgURL)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.avatr);
    }

    private String getImageHash(String memberID) {
        MemberModel memberRealm = realm.where(MemberModel.class).equalTo("memberID", memberID).findFirst();
        if (memberRealm != null) {
            return memberRealm.getHashAvatar();
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return card.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView avatr;

        ViewHolder(View itemView) {
            super(itemView);
            avatr = itemView.findViewById(R.id.avatr);
        }
    }
}