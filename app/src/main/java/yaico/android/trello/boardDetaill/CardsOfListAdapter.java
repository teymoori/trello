package yaico.android.trello.boardDetaill;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import yaico.android.trello.R;
import yaico.android.trello.utils.Gen;
import yaico.android.trello.utils.views.MyTextView;
import yaico.android.trello.utils.webmodels.CardModel;

/**
 * Created by amirhossein on 12/16/17.
 */

public class CardsOfListAdapter extends RecyclerView.Adapter<CardsOfListAdapter.ViewHolder> {

    private List<CardModel> card;
    Context mContext;

    public CardsOfListAdapter(List<CardModel> card) {
        this.card = card;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = inflater.inflate(R.layout.card_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.cardTitle.setText(card.get(position).getName());
        if (card.get(position).getDesc() != null && card.get(position).getDesc().length() > 0) {
            holder.desc.setText(card.get(position).getDesc());
            holder.desc.setBackground(ContextCompat.getDrawable(mContext , R.drawable.green_frame));
        } else {
            holder.desc.setBackground(ContextCompat.getDrawable(mContext , R.drawable.red_frame));
        }


        holder.shamsiDate.setText(Gen.getShamsiDateByDateTime(card.get(position).getDateLastActivity()));
        holder.miladiDate.setText(Gen.getGregorianDateByDateTime(card.get(position).getDateLastActivity()));
        CardMembersAdapter adapter = new CardMembersAdapter(card.get(position).getIdMembers());
        holder.members.setAdapter(adapter);
        holder.members.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    public int getItemCount() {
        return card.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        MyTextView cardTitle;
        MyTextView desc;
        MyTextView shamsiDate;
        MyTextView miladiDate;
        RecyclerView members;

        ViewHolder(View itemView) {
            super(itemView);
            cardTitle = itemView.findViewById(R.id.cardTitle);
            members = itemView.findViewById(R.id.members);
            shamsiDate = itemView.findViewById(R.id.shamsiDate);
            miladiDate = itemView.findViewById(R.id.miladiDate);
            desc = itemView.findViewById(R.id.desc);
        }
    }
}