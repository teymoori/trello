package yaico.android.trello.boardDetaill;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.List;

import yaico.android.trello.R;
import yaico.android.trello.utils.views.MyTextView;
import yaico.android.trello.utils.webmodels.PList;

/**
 * Created by amirhossein on 12/16/17.
 */

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.ViewHolder> {

    private List<PList> data;
    private Context mContext;

    DetailAdapter(List<PList> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = inflater.inflate(R.layout.project_tasks_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int count = 0;
        if (data.get(position).getCards() != null) {
            count = data.get(position).getCards().size();
        }
        holder.taskGroupTitle.setText(data.get(position).getName() + " (" + count + ")");
        //internal list of cards
        CardsOfListAdapter adapter = new CardsOfListAdapter(data.get(position).getCards());
        holder.cardsList.setAdapter(adapter);
        holder.cardsList.setLayoutManager(new LinearLayoutManager(mContext));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        CardView bgAll;
        MyTextView taskGroupTitle;
        RecyclerView cardsList;

        ViewHolder(View itemView) {
            super(itemView);

            bgAll = itemView.findViewById(R.id.bgAll);
            taskGroupTitle = itemView.findViewById(R.id.taskGroupTitle);
            cardsList = itemView.findViewById(R.id.cardsList);
        }
    }
}