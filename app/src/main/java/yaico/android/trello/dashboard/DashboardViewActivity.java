package yaico.android.trello.dashboard;


import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.List;

import yaico.android.trello.R;

import yaico.android.trello.boardDetaill.BoardDetailActivity_;
import yaico.android.trello.timeDetail.TimeDetailViewActivity_;
import yaico.android.trello.utils.BaseActivity;
import yaico.android.trello.utils.Gen;
import yaico.android.trello.utils.webmodels.BoardsModel;


@EActivity(R.layout.dashboard_view_activity)
public class DashboardViewActivity extends BaseActivity implements DashboardContract.View, DashboardRecyclerBoardsAdapter.ItemClickListener {
    DashboardContract.Presenter presenter;
    @ViewById
    RecyclerView boardList;
    @ViewById
    SwipeRefreshLayout swipe_layout;

    @AfterInject
    void afterInject() {
        presenter = new DashboardPresenter();
    }

    @StringRes
    String sign_out;

    @AfterViews
    void afterViews() {
        presenter.attachView(this);
        presenter.onScreenShow();
        swipe_layout.setOnRefreshListener(this::afterSwipe);
    }


    @Click
    void update() {
        presenter.onUpdateClicked();
    }


    @Override
    @UiThread
    public void onDataLoaded(List<BoardsModel> results) {
        DashboardRecyclerBoardsAdapter adapter = new DashboardRecyclerBoardsAdapter(mContext, results);
        boardList.setLayoutManager(new GridLayoutManager(mContext, 2));
        adapter.setItemClickListener(DashboardViewActivity.this);
        boardList.setAdapter(adapter);
    }

    @Override
    @UiThread
    public void onDataLoadError(String msg) {
        Gen.showToast(mContext, msg);
        onProgress(false);
    }

    @Override
    @UiThread
    public void onProgress(Boolean show) {
        swipe_layout.setRefreshing(show);
    }

    @Override
    public void goToDetailActivity(BoardsModel boardsModel) {
        BoardDetailActivity_.intent(mContext).setShortLink(boardsModel.getShortLink()).start();
    }

    @Click
    void getTimes() {
        presenter.onTimeDetailSelect();
    }

    @Override
    public void goToTimeDetail() {
        Intent timeDetailIntent = new Intent(mContext, TimeDetailViewActivity_.class);
        startActivity(timeDetailIntent);
    }


    private void afterSwipe() {
        presenter.onUpdateClicked();
    }

    @Override
    public void ItemClicked(BoardsModel item) {
        presenter.onBoardSelect(item);
    }

    @Click
    void menu() {
        this.openOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, sign_out);
        return super.onCreateOptionsMenu(menu);
    }
}
