package yaico.android.trello.dashboard;

import java.util.List;

import yaico.android.trello.utils.webmodels.BoardsModel;

/**
 * Created by amirhossein on 12/12/17.
 */

public interface DashboardContract {

    //responses from presenter
    interface View {
        void onDataLoaded(List<BoardsModel> board);

        void onDataLoadError(String msg);

        void onProgress(Boolean show);

        void goToDetailActivity(BoardsModel boardsModel);
        void goToTimeDetail( );

    }

    //requests from activity
    interface Presenter {
        void attachView(View view);

        void onScreenShow();

        void onUpdateClicked();

        void loadData(List<BoardsModel> board);

        void onBoardSelect(BoardsModel board);

        void showError(String msg);
        void onTimeDetailSelect();
    }
}
