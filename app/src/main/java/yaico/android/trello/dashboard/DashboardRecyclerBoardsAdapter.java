package yaico.android.trello.dashboard;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import yaico.android.trello.R;
import yaico.android.trello.boardDetaill.CardMembersAdapter;
import yaico.android.trello.utils.webmodels.BoardsModel;
import yaico.android.trello.utils.webmodels.Membership;

/**
 * Created by amirhossein on 12/13/17.
 */

public class DashboardRecyclerBoardsAdapter extends RecyclerView.Adapter<DashboardRecyclerBoardsAdapter.ViewHolder> {

    private Context mContext;
    private List<BoardsModel> boards;

    DashboardRecyclerBoardsAdapter(Context mContext, List<BoardsModel> boards) {
        this.mContext = mContext;
        this.boards = boards;
    }

    @Override
    public DashboardRecyclerBoardsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.board_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DashboardRecyclerBoardsAdapter.ViewHolder holder, int position) {

        holder.bgAll.setBackgroundColor(Color.parseColor(boards.get(position).getPrefs().getBackgroundBottomColor()));

        holder.img.setVisibility(View.INVISIBLE);
        holder.tinter.setVisibility(View.INVISIBLE);

        if (boards.get(position).getPrefs().getBackgroundImageScaled() != null)
            if (boards.get(position).getPrefs().getBackgroundImageScaled().get(0) != null) {
                holder.img.setVisibility(View.VISIBLE);
                holder.tinter.setVisibility(View.VISIBLE);
                Glide.with(mContext).load(boards.get(position).getPrefs().getBackgroundImageScaled().get(2).getUrl())
                        .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img);
            }


        holder.title.setText(String.format("%s (%d) ", boards.get(position).getName(), boards.get(position).getMemberships().size()));


        List<String> membersIDs = new ArrayList<>();
        for (Membership member : boards.get(position).getMemberships()) {
            membersIDs.add(member.getIdMember());
        }
        if (membersIDs.size() > 0){
            CardMembersAdapter adapter = new CardMembersAdapter(membersIDs) ;
            holder.members.setAdapter(adapter);
            holder.members.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        }


    }

    void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    private ItemClickListener itemClickListener;

    public interface ItemClickListener {
        void ItemClicked(BoardsModel item);
    }

    @Override
    public int getItemCount() {
        return boards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView img;
        TextView title;
        View bgAll;
        RecyclerView members;

        View tinter;

        ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img);
            title = itemView.findViewById(R.id.title);
            bgAll = itemView.findViewById(R.id.bgAll);
            members = itemView.findViewById(R.id.members);
            tinter = itemView.findViewById(R.id.tinter);
            bgAll.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.ItemClicked(boards.get(getAdapterPosition()));
        }
    }

}
