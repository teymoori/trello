package yaico.android.trello.dashboard;

import android.util.Log;

import java.util.List;

import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import yaico.android.trello.utils.BaseClass;
import yaico.android.trello.utils.Gen;
import yaico.android.trello.utils.MyApplication;
import yaico.android.trello.utils.webmodels.BoardDBModel;
import yaico.android.trello.utils.webmodels.BoardsModel;
import yaico.android.trello.utils.webmodels.MemberDetailModel;
import yaico.android.trello.utils.webmodels.MemberModel;
import yaico.android.trello.utils.webmodels.Membership;

/**
 * Created by amirhossein on 12/12/17.
 */


class DashboardModel extends BaseClass {

    private DashboardPresenter presenter;
    Realm realm;

    DashboardModel(DashboardPresenter presenter) {
        this.presenter = presenter;
    }

    void getProjectModel() {

        MyApplication.trello.getBoards(Gen.getKey(), Gen.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(this::onDataReceived, this::onError, this::onComplete);
    }

    private void onError(Throwable throwable) {
        presenter.showError(throwable.toString());
    }

    private void handleHashAvatar(Membership membership) {
        if (realm == null) {
            realm = Realm.getDefaultInstance();
        }

        MemberDetailModel memberDetail = null;
        MemberModel memberModel = null;
        try {


            MemberModel memberRealm = realm.where(MemberModel.class)
                    .equalTo("memberID", membership.getIdMember()).findFirst();

            if (memberRealm == null) {
                memberDetail = MyApplication.trello.getUserDetails(membership.getIdMember(), Gen.getKey(), Gen.getToken()).execute().body();
                Log.d(TAG, "handleHashAvatar0: " + memberDetail.toString());
                if (memberDetail != null) {
                    memberModel = new MemberModel();
                    memberModel.setHashAvatar(memberDetail.getAvatarHash());
                    memberModel.setMemberID(memberDetail.getId());
                    memberModel.setUserFamily(memberDetail.getFullName());
                    memberModel.setUserName(memberDetail.getUsername());
                    memberModel.setMemberAvatarURL(Gen.getGravatarAddress(memberDetail.getId()));
                    realm.beginTransaction();
                    realm.copyToRealm(memberModel);
                    realm.commitTransaction();
                }
            }
            Log.d(TAG, "handleHashAvatar1: " + memberModel.toString());

        } catch (Exception e) {
            Log.d(TAG, "handleHashAvatar2: " + e);
        }
    }

    private void onDataReceived(List<BoardsModel> boardsModel) {
        for (BoardsModel bord : boardsModel) {
            for (Membership member : bord.getMemberships()) {
                handleHashAvatar(member);
            }
        }
        saveBoardsInDB(boardsModel);
        presenter.loadData(boardsModel);
    }

    private void saveBoardsInDB(List<BoardsModel> boardsModels) {
        if (realm == null) {
            realm = Realm.getDefaultInstance();
        }
        //clean old boards
        realm.beginTransaction();
        realm.delete(BoardDBModel.class);
        realm.commitTransaction();

        //add new boards to db
        for (BoardsModel board : boardsModels) {
            BoardDBModel dbModel = new BoardDBModel(board.getName(), board.getId(), board.getShortLink());
            realm.beginTransaction();
            realm.copyToRealm(dbModel);
            realm.commitTransaction();
        }

    }

    private void onComplete() {
        Log.d(TAG, "onComplete");
    }


}
