package yaico.android.trello.dashboard;

import java.util.List;

import yaico.android.trello.utils.webmodels.BoardsModel;

/**
 * Created by amirhossein on 12/12/17.
 */

public class DashboardPresenter implements DashboardContract.Presenter {


    private DashboardContract.View view;
    private DashboardModel model;

    @Override
    public void attachView(DashboardContract.View view) {
        this.view = view;
        model = new DashboardModel(this);
    }

    @Override
    public void onScreenShow() {
        view.onProgress(true);
        model.getProjectModel();
    }

    @Override
    public void onUpdateClicked() {
        view.onProgress(true);
        model.getProjectModel();
    }

    @Override
    public void loadData(List<BoardsModel> results) {
        view.onDataLoaded(results);
        view.onProgress(false);
    }

    @Override
    public void onBoardSelect(BoardsModel boardsModel) {
        view.goToDetailActivity(boardsModel);
    }

    @Override
    public void showError(String msg) {
        view.onDataLoadError(msg);
        view.onProgress(false);
    }

    @Override
    public void onTimeDetailSelect() {
        view.goToTimeDetail();
    }
}
