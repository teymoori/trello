package yaico.android.trello.login;

import android.content.Intent;
import android.net.Uri;

import yaico.android.trello.utils.BaseClass;
import yaico.android.trello.utils.Constants;
import yaico.android.trello.utils.webmodels.ProjectListModel;

/**
 * Created by amirhossein on 12/19/17.
 */

public class LoginPresenter extends BaseClass implements LoginContract.Presenter {

    LoginContract.View view;
    UserModel model = new UserModel(this);

    public LoginPresenter(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void attachView(LoginContract.View view) {

    }

    @Override
    public void onScreenShow() {
        if (model.getToken() != null) {
            view.goToDashboard();
            view.onFinish();
        }
    }


    @Override
    public void showError() {
        view.tokenInvalid();
    }

    @Override
    public void loadData(ProjectListModel projectListModel) {

    }

    @Override
    public void onBackClicked() {

    }

    @Override
    public void login(String token) {
        if ((token != null ? token.length() : 0) >= 64) {
            model.tokenValidate(token);
        } else
            view.tokenInvalid();
    }

    @Override
    public void getAuthorize() {
        Intent tIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.trelloGetTokenURL));
        view.onAuthorize(tIntent);
    }

    @Override
    public void onResume() {

    }
}
