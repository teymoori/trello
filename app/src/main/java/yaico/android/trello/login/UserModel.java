package yaico.android.trello.login;

import android.util.Log;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import yaico.android.trello.utils.BaseClass;
import yaico.android.trello.utils.Gen;
import yaico.android.trello.utils.MyApplication;
import yaico.android.trello.utils.webmodels.BoardsModel;

/**
 * Created by amirhossein on 12/19/17.
 */

public class UserModel extends BaseClass {
    LoginPresenter presenter;
    private String token;

    public UserModel(LoginPresenter presenter) {
        this.presenter = presenter;
    }


    public String getToken() {
        return Gen.getToken();
    }

    public Boolean tokenValidate(String token) {
        this.token = token;
        MyApplication.trello.getBoards(Gen.getKey(), token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onDataReceived, this::onError, this::onComplete);


        return false;

    }

    private void onComplete() {
        Gen.setToken(token);
        presenter.onScreenShow();
    }

    private void onError(Throwable throwable) {
        presenter.showError();
    }

    private void onDataReceived(List<BoardsModel> boardsModels) {
    }


}
