package yaico.android.trello.login;

import android.content.Intent;

import yaico.android.trello.boardDetaill.BoardDetailContract;
import yaico.android.trello.utils.webmodels.ProjectListModel;

/**
 * Created by amirhossein on 12/19/17.
 */

public interface LoginContract {

    interface View {

        void onDataLoaded(ProjectListModel projectListModel);
        void onDataLoadError(     );

        void onProgress(Boolean show);
        void goToDashboard();
        void onFinish() ;

        void tokenInvalid();
        void onAuthorize(Intent tIntent);
    }


    //requests from activity
    interface Presenter {
        void attachView(LoginContract.View view);

        void onScreenShow( );

        void showError(   );
        void loadData(ProjectListModel projectListModel);
        void onBackClicked() ;

        void login(String token);
        void getAuthorize() ;
        void onResume() ;
    }
}
