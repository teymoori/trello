package yaico.android.trello.login;

import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Half;

import com.orhanobut.hawk.Hawk;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import yaico.android.trello.R;
import yaico.android.trello.dashboard.DashboardViewActivity_;
import yaico.android.trello.utils.BaseActivity;
import yaico.android.trello.utils.Constants;
import yaico.android.trello.utils.Gen;
import yaico.android.trello.utils.views.MyEditText;
import yaico.android.trello.utils.webmodels.ProjectListModel;

@EActivity(R.layout.activity_login_view)
public class LoginViewActivity extends BaseActivity implements LoginContract.View {
    LoginContract.Presenter presenter;
    int webIntentCode = 100;

    @StringRes
    String token_invalid;

    @ViewById
    TextInputEditText token;

    @AfterViews
    void initViews() {
        presenter = new LoginPresenter(this);
        presenter.onScreenShow();
        //  Hawk.put("token", "cf33550a4fafdb2c3557aa55eab659dd185cf6bd372ac54cb0a17c2df38a3fee");
    }

    @Override
    public void onDataLoaded(ProjectListModel projectListModel) {
    }

    @Override
    public void goToDashboard() {
        startActivity(new Intent(mContext, DashboardViewActivity_.class));
    }

    @Override
    public void onDataLoadError() {
    }

    @Override
    public void onProgress(Boolean show) {
    }

    @Override
    public void onFinish() {
        finish();
    }

    @Click
    void openAuth() {
        presenter.getAuthorize();
    }

    @Click
    void login() {
        presenter.login(token.getText().toString());
    }

    @Override
    public void tokenInvalid() {
        token.setError(token_invalid);
    }

    @Override
    public void onAuthorize(Intent tIntent) {
        startActivityForResult(tIntent, webIntentCode);
    }


}
