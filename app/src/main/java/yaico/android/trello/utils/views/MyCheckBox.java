package yaico.android.trello.utils.views;

import android.content.Context;
import android.util.AttributeSet;

import com.rey.material.widget.CheckBox;

import yaico.android.trello.utils.MyApplication;



/**
 * Created by info on 3/12/2017.11
 */

public class MyCheckBox extends CheckBox {

    public MyCheckBox(Context context) {
        super(context);
//        if (!isInEditMode())
        this.setTypeface(MyApplication.mTypeface);
    }

    public MyCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
//        if (!isInEditMode())
        this.setTypeface(MyApplication.mTypeface);
    }

}
