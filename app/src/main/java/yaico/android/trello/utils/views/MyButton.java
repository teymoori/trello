package yaico.android.trello.utils.views;

import android.content.Context;
import android.util.AttributeSet;

import yaico.android.trello.utils.MyApplication;


/**
 * Created by info on 9/30/2016.
 */

public class MyButton extends com.rey.material.widget.Button {

    public MyButton(Context context) {
        super(context);
//        if (!isInEditMode())
            this.setTypeface(MyApplication.mTypeface);
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
//        if (!isInEditMode())
            this.setTypeface(MyApplication.mTypeface);
    }
}
