package yaico.android.trello.utils;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by amirhossein on 12/13/17.
 */

public class Gen extends BaseClass {

    public static void showToast(Context mContext, String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }


    public static String getShamsiDateByDateTime(String dateTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Date date = dateFormat.parse(dateTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            CalendarTool cal = new CalendarTool(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            return cal.getIranianDate();
        } catch (Exception e) {
        }
        return dateTime;
    }

    public static String getGregorianDateByDateTime(String dateTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Date date = dateFormat.parse(dateTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            CalendarTool cal = new CalendarTool(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            return cal.getGregorianDate();
        } catch (Exception e) {
        }
        return dateTime;
    }


    public static String getGravatarAddress(String imgHash) {
        return "http://trello-avatars.s3.amazonaws.com/" + imgHash + "/170.png";
    }

    public static void initVDiscreteScrollView(DiscreteScrollView list) {
        list.setOffscreenItems(10);
        list.setItemTransitionTimeMillis(300);
        list.setItemTransformer(new ScaleTransformer.Builder()
                .setMaxScale(1.00f)
                .setMinScale(0.9f)
                .setPivotX(Pivot.X.CENTER)
                .setPivotY(Pivot.Y.BOTTOM)
                .build());
    }


    public static String getToken() {
        return Hawk.get("token");
    }

    public static void setToken(String token) {
        Hawk.put("token", token);
    }

    public static String getKey() {
        return Hawk.get("key");
    }



}
