
package yaico.android.trello.utils.webmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Orgs {

    @SerializedName("totalPerMember")
    @Expose
    private TotalPerMember_ totalPerMember;

    public TotalPerMember_ getTotalPerMember() {
        return totalPerMember;
    }

    public void setTotalPerMember(TotalPerMember_ totalPerMember) {
        this.totalPerMember = totalPerMember;
    }

}
