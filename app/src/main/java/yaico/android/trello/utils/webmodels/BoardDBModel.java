package yaico.android.trello.utils.webmodels;

import io.realm.RealmObject;

/**
 * Created by amirhossein on 12/20/17.
 */

public class BoardDBModel extends RealmObject{

    String name;
    String id ;
    String shortLink ;


    public BoardDBModel() {
    }

    public BoardDBModel(String name, String id, String shortLink) {
        this.name = name;
        this.id = id;
        this.shortLink = shortLink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortLink() {
        return shortLink;
    }

    public void setShortLink(String shortLink) {
        this.shortLink = shortLink;
    }
}
