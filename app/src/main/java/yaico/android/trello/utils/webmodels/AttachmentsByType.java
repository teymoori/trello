
package yaico.android.trello.utils.webmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttachmentsByType {

    @SerializedName("trello")
    @Expose
    private Trello trello;

    public Trello getTrello() {
        return trello;
    }

    public void setTrello(Trello trello) {
        this.trello = trello;
    }

}
