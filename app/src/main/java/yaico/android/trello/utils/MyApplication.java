package yaico.android.trello.utils;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import com.orhanobut.hawk.Hawk;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import yaico.android.trello.utils.interfaces.Trello;

/**
 * Created by amirhossein on 12/16/17.
 */

public class MyApplication extends Application {
    public static Trello trello = RetrofitServiceGenerator.createService(Trello.class);
    public static Typeface mTypeface = null;
    public static Context mContext ;

    public void setTypeface(Context mContext) {
        if (mTypeface == null)
            mTypeface = Typeface.createFromAsset(mContext.getAssets(), Constants.fontName);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        setTypeface(this);
        Realm.init(this);
        Hawk.init(this).build();
        mContext = this ;
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("jt.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

        Hawk.put("key", Constants.trelloKey);

        Log.d("app_debug", "key   : " + Gen.getKey());
        Log.d("app_debug", "token : " + Gen.getToken());


    }


}
