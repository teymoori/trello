package yaico.android.trello.utils.views;

import android.content.Context;
import android.util.AttributeSet;

import yaico.android.trello.utils.MyApplication;


/**
 * Created by info on 9/30/2016.
 */

public class MyEditText extends android.support.v7.widget.AppCompatEditText {
    Context mContext;

    public MyEditText(Context context) {
        super(context);
        mContext = context;
        if (!isInEditMode())
            this.setTypeface(MyApplication.mTypeface);
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        if (!isInEditMode())
            this.setTypeface(MyApplication.mTypeface);
    }


    @Override
    public void setText(CharSequence text, BufferType type) {
        try {
            String t = text.toString();
            super.setText((CharSequence) t, type);
        } catch (Exception e) {
        }
    }
}