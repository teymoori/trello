
package yaico.android.trello.utils.webmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProjectListModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lists")
    @Expose
    private java.util.List<PList> lists = null;


    private java.util.List<CardModel> cards = null;


    public List<CardModel> getCards() {
        return cards;
    }

    public void setCards(List<CardModel> cards) {
        this.cards = cards;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public java.util.List<PList> getLists() {
        return lists;
    }

    public void setLists(java.util.List<PList> lists) {
        this.lists = lists;
    }

}
