
package yaico.android.trello.utils.webmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Limits {

    @SerializedName("attachments")
    @Expose
    private Attachments attachments;
    @SerializedName("checklists")
    @Expose
    private Checklists checklists;
    @SerializedName("stickers")
    @Expose
    private Stickers stickers;

    public Attachments getAttachments() {
        return attachments;
    }

    public void setAttachments(Attachments attachments) {
        this.attachments = attachments;
    }

    public Checklists getChecklists() {
        return checklists;
    }

    public void setChecklists(Checklists checklists) {
        this.checklists = checklists;
    }

    public Stickers getStickers() {
        return stickers;
    }

    public void setStickers(Stickers stickers) {
        this.stickers = stickers;
    }

}
