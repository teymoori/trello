package yaico.android.trello.utils.webmodels;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by amirhossein on 12/17/17.
 */

public class MemberModel extends RealmObject {


    String memberID;
    String UserFamily;
    String userName;
    String hashAvatar;
    String memberAvatarURL;

    public MemberModel() {
    }


    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getUserFamily() {
        return UserFamily;
    }

    public void setUserFamily(String userFamily) {
        UserFamily = userFamily;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHashAvatar() {
        return hashAvatar;
    }

    public void setHashAvatar(String hashAvatar) {
        this.hashAvatar = hashAvatar;
    }

    public String getMemberAvatarURL() {
        return memberAvatarURL;
    }

    public void setMemberAvatarURL(String memberAvatarURL) {
        this.memberAvatarURL = memberAvatarURL;
    }
}
