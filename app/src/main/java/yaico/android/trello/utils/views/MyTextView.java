package yaico.android.trello.utils.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import yaico.android.trello.utils.MyApplication;


/**
 * Created by info on 9/30/2016.
 */

public class MyTextView extends com.rey.material.widget.TextView {

    public MyTextView(Context context) {
        super(context);
        if (!isInEditMode())
            this.setTypeface(MyApplication.mTypeface);

    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            this.setTypeface(MyApplication.mTypeface);
    }



}
