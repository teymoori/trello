
package yaico.android.trello.utils.webmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attachments {

    @SerializedName("perCard")
    @Expose
    private PerCard perCard;

    public PerCard getPerCard() {
        return perCard;
    }

    public void setPerCard(PerCard perCard) {
        this.perCard = perCard;
    }

}
