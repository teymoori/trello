
package yaico.android.trello.utils.webmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stickers {

    @SerializedName("perCard")
    @Expose
    private PerCard__ perCard;

    public PerCard__ getPerCard() {
        return perCard;
    }

    public void setPerCard(PerCard__ perCard) {
        this.perCard = perCard;
    }

}
