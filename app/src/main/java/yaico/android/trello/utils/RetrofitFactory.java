package yaico.android.trello.utils;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by amirhossein on 12/12/17.
 */

public class RetrofitFactory<T> {
    private final static String mainUrl = Constants.BASE_URL;
    public static <T> T createRequest(final Class<T> tClass) {
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mainUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(tClass);

    }
}