package yaico.android.trello.utils.interfaces;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import io.reactivex.Observable;
import retrofit2.http.Path;
import retrofit2.http.Query;
import yaico.android.trello.utils.webmodels.BoardsModel;
import yaico.android.trello.utils.webmodels.CardModel;
import yaico.android.trello.utils.webmodels.MemberDetailModel;
import yaico.android.trello.utils.webmodels.ProjectListModel;

/**
 * Created by amirhossein on 12/12/17.
 */

public interface Trello {
    @GET("members/me/boards")
    Observable<List<BoardsModel>> getBoards(@Query("key") String key, @Query("token") String token);


    @GET("boards/{shortLink}?fields=id,name&lists=open&list_fields=id,name,closed,pos")
    Observable<ProjectListModel> getListsOfProject(@Path("shortLink") String shortLink , @Query("key") String key, @Query("token") String token);


    @GET("lists/{listID}/cards?fields=all")
    Call<ArrayList<CardModel>> getCardsOfListID(@Path("listID") String listID, @Query("key") String key, @Query("token") String token);


    @GET("members/{MemberID}?fields=all")
    Call<MemberDetailModel> getUserDetails(@Path("MemberID") String MemberID, @Query("key") String key, @Query("token") String token);


}
