package yaico.android.trello.utils;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by a.teymoori on 5/1/2017.
 */

public class RetrofitServiceGenerator {


    private static OkHttpClient httpClient =
            new OkHttpClient.Builder().readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS).build();

    static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();


    private static Retrofit.Builder builder =


            new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                .build();

        return retrofit.create(serviceClass);
    }


//    public static <S> S createServiceWithAuth(Class<S> serviceClass) {
//        Interceptor interceptor = new Interceptor() {
//            @Override
//            public Response intercept(Chain chain) throws IOException {
//                Request request = chain.request().newBuilder().build();
//                try {
//                    request = chain.request().newBuilder()
//                            .addHeader("mobile", Gen.getMobile())
//                            .addHeader("token", Gen.getToken())
//                            .build();
//                } catch (Exception e) {
//                }
//                return chain.proceed(request);
//            }
//        };
//        HttpLoggingInterceptor interceptorLogging = new HttpLoggingInterceptor();
//        interceptorLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptorLogging).addInterceptor(interceptor).readTimeout(30, TimeUnit.SECONDS)
//                .connectTimeout(30, TimeUnit.SECONDS).build();
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(API_BASE_URL)
//                .client(client)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        return retrofit.create(serviceClass);
//    }

}
