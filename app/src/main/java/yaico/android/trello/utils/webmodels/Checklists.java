
package yaico.android.trello.utils.webmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Checklists {

    @SerializedName("perCard")
    @Expose
    private PerCard_ perCard;

    public PerCard_ getPerCard() {
        return perCard;
    }

    public void setPerCard(PerCard_ perCard) {
        this.perCard = perCard;
    }

}
