
package yaico.android.trello.utils.webmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TotalPerMember_ {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("disableAt")
    @Expose
    private Integer disableAt;
    @SerializedName("warnAt")
    @Expose
    private Integer warnAt;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getDisableAt() {
        return disableAt;
    }

    public void setDisableAt(Integer disableAt) {
        this.disableAt = disableAt;
    }

    public Integer getWarnAt() {
        return warnAt;
    }

    public void setWarnAt(Integer warnAt) {
        this.warnAt = warnAt;
    }

}
