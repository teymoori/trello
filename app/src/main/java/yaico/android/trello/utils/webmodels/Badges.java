
package yaico.android.trello.utils.webmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Badges {

    @SerializedName("votes")
    @Expose
    private Integer votes;
    @SerializedName("attachmentsByType")
    @Expose
    private AttachmentsByType attachmentsByType;
    @SerializedName("viewingMemberVoted")
    @Expose
    private Boolean viewingMemberVoted;
    @SerializedName("subscribed")
    @Expose
    private Boolean subscribed;
    @SerializedName("fogbugz")
    @Expose
    private String fogbugz;
    @SerializedName("checkItems")
    @Expose
    private Integer checkItems;
    @SerializedName("checkItemsChecked")
    @Expose
    private Integer checkItemsChecked;
    @SerializedName("comments")
    @Expose
    private Integer comments;
    @SerializedName("attachments")
    @Expose
    private Integer attachments;
    @SerializedName("description")
    @Expose
    private Boolean description;
    @SerializedName("due")
    @Expose
    private Object due;
    @SerializedName("dueComplete")
    @Expose
    private Boolean dueComplete;

    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public AttachmentsByType getAttachmentsByType() {
        return attachmentsByType;
    }

    public void setAttachmentsByType(AttachmentsByType attachmentsByType) {
        this.attachmentsByType = attachmentsByType;
    }

    public Boolean getViewingMemberVoted() {
        return viewingMemberVoted;
    }

    public void setViewingMemberVoted(Boolean viewingMemberVoted) {
        this.viewingMemberVoted = viewingMemberVoted;
    }

    public Boolean getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(Boolean subscribed) {
        this.subscribed = subscribed;
    }

    public String getFogbugz() {
        return fogbugz;
    }

    public void setFogbugz(String fogbugz) {
        this.fogbugz = fogbugz;
    }

    public Integer getCheckItems() {
        return checkItems;
    }

    public void setCheckItems(Integer checkItems) {
        this.checkItems = checkItems;
    }

    public Integer getCheckItemsChecked() {
        return checkItemsChecked;
    }

    public void setCheckItemsChecked(Integer checkItemsChecked) {
        this.checkItemsChecked = checkItemsChecked;
    }

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }

    public Integer getAttachments() {
        return attachments;
    }

    public void setAttachments(Integer attachments) {
        this.attachments = attachments;
    }

    public Boolean getDescription() {
        return description;
    }

    public void setDescription(Boolean description) {
        this.description = description;
    }

    public Object getDue() {
        return due;
    }

    public void setDue(Object due) {
        this.due = due;
    }

    public Boolean getDueComplete() {
        return dueComplete;
    }

    public void setDueComplete(Boolean dueComplete) {
        this.dueComplete = dueComplete;
    }

}
