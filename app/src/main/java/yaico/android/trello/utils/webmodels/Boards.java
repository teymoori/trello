
package yaico.android.trello.utils.webmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Boards {

    @SerializedName("totalPerMember")
    @Expose
    private TotalPerMember totalPerMember;

    public TotalPerMember getTotalPerMember() {
        return totalPerMember;
    }

    public void setTotalPerMember(TotalPerMember totalPerMember) {
        this.totalPerMember = totalPerMember;
    }

}
